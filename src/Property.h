/*
 * property.h
 *
 *  Created on: May 16, 2015
 *      Author: jonesey
 */

#ifndef PROPERTY_H_
#define PROPERTY_H_

template<typename T, int capacity>
class Property
{
public:
	//default constructor
	Property();

	//construct an aether object that is m x n
	Property(int m,int n);

	//return the population of the aether at location (x, y)
	T getValue(int x, int y);

	//set population of the aether at location (x, y)
	void setValue(int x, int y, T val);

	//set width of array
	void setWidth(int x);

	//set height of array
	void setHeight(int y);

	//increments a value
	void increment(int x, int y);

	//decrements a value
	void decrement(int x, int y);

private:
	T** value;
	int width;
	int height;

	void initializeProperty(int x, int y);

};



#endif /* PROPERTY_H_ */
