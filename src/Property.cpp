/*
 * Properties.cpp
 *
 *  Created on: May 18, 2015
 *      Author: jonesey
 */

#include "Property.h"
//#include "globals.h"
#include "arraySize.h"

template <typename T, int capacity>
Property<T, capacity>::Property(){
	int defaultSize = capacity;

	this->setWidth(defaultSize);
	this->setHeight(defaultSize);

	initializeProperty(defaultSize, defaultSize);

	for (int i = 0; i < defaultSize; i++){
		for (int j = 0; j < defaultSize; j++){
			//might have to change this for template...
			this->setValue(i,j,0);
		} //endfor j
	}//endfor i

}

template <typename T, int capacity>
void Property<T, capacity>::initializeProperty(int _x, int _y){

	this->value = new T* [_x];
	for (int i = 0; i < _x; i++)
		value[i] = new T[_y];


}

template <typename T, int capacity>
void Property<T, capacity>::setValue(int _x, int _y, T _val){

	this->value[_x][_y]= _val;
}

template <typename T, int capacity>
T Property<T, capacity>::getValue(int _x, int _y){

	return this->value[_x][_y];
}

template <typename T, int capacity>
void Property<T, capacity>::setWidth(int _x){

	this->width = _x;
}

template <typename T, int capacity>
void Property<T,capacity>::setHeight(int _y){

	this->height = _y;
}

//note: I should create a sub-class of property for integer containers as these don't make sense
//for floats or other things...
template <typename T, int capacity>
void Property<T, capacity>::increment(int _x, int _y){

	this->value[_x][_y]++;
};

//note: I should create a sub-class of property for integer containers as these don't make sense
//for floats or other things...
template <typename T, int capacity>
void Property<T, capacity>::decrement(int _x, int _y){
	if(this->value[_x][_y]>0)
		this->value[_x][_y]--;
};



//Class Templates - c++ artifact :P

template class Property<int, global_x_dim>;
template class Property<float, global_x_dim>;
//template class Property<bool, global_x_dim>;


//NEED TO ADD DECONSTRUCTOR




