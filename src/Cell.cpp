/*
 * Cell.cpp
 *
 *  Created on: May 22, 2015
 *      Author: jonesey
 */

#include "Cell.h"

//default constructor
Cell::Cell(){
	this->x = 1;
	this->y = 1;
	this->state = true;
	this->lifeForce = 0.0;
	};
//x, y constructor
Cell::Cell(int _x, int _y){
	this->x = _x;
	this->y = _y;
	this->state = true;
	this->lifeForce = 0.0;
	};

//x, y, state constructor
Cell::Cell(int _x, int _y, bool _state){
	this->x = 1;
	this->y = 1;
	this->state = _state;
	this->lifeForce = 0.0;
	};


void Cell::live(){

	this->state = true;

};

void Cell::sleep(){

	this->state = false;

};

int Cell::checkNeighbors(int _x, int _y, Aether _myAether){

	return _myAether.getNeighbors(_x, _y);

}

int Cell::checkPopulation(int _x, int _y, Aether _myAether){

	return _myAether.getPopulation(_x, _y);

}

float Cell::checkChangeValue(int _x, int _y, Aether _myAether){

	return _myAether.getChangeValue(_x, _y);

}

void Cell::setLifeForce(float _value){

	this->lifeForce += _value;

	return;
}

void Cell::reduceLifeForce(float _change){

	float currentLife = this->getLifeForce();
	this->setLifeForce(currentLife+100*_change);
	this->lifeForce -= _change;
	//if (_change < 0.0000001) this->setLifeForce(0.0);
}

float Cell::getLifeForce(){

	return this->lifeForce;
}

int Cell::getX(){

	return this->x;
}

int Cell::getY(){

	return this->y;

}


void Cell::linOp(int _x, int _y, Aether _myAether){

	//if(_myAether.getImmunity(_x,_y)==1) std::cout << "i" << _x;

	if(_myAether.getImmunity(_x,_y)!=1){

	float oldValue = 0.0;
	float change = 0.0;
	float value = 0.0;
	oldValue = _myAether.getFieldValue(_x, _y);

	//this uses a finite difference formulation of Poisson's equation
	//and should be updated to call an arbitrary linear operator to
	//solve problems other than relaxation

	//note: the basic operator uses a '+' pattern and is invalid at edges.
	//There will be five cases to account for edges
	//although there may be a better way...

/*	if(_x == 0){

		value = (_myAether.getFieldValue(_x+1, _y)+
				 _myAether.getFieldValue(_x, _y-1)+
				 _myAether.getFieldValue(_x, _y+1))/3;
			//std::cout << "L";
	}
	else if (_y == 0){
		value = (_myAether.getFieldValue(_x-1, _y)+
				 _myAether.getFieldValue(_x+1, _y)+
				 _myAether.getFieldValue(_x, _y+1))/3;
			//std::cout << "T";
	}
	else if (_x == global_x_dim -1){

		value = (_myAether.getFieldValue(_x-1, _y)+
				 _myAether.getFieldValue(_x, _y-1)+
				 _myAether.getFieldValue(_x, _y+1))/3;
		 	//std::cout << "R";
	}
	else if (_y == global_y_dim -1){

		value = (_myAether.getFieldValue(_x-1, _y)+
				 _myAether.getFieldValue(_x+1, _y)+
				 _myAether.getFieldValue(_x, _y-1))/3;
			//std::cout << "B";
	}

	else{
*/
	value = (_myAether.getFieldValue(_x-1, _y)+
			 _myAether.getFieldValue(_x+1, _y)+
			 _myAether.getFieldValue(_x, _y-1)+
			 _myAether.getFieldValue(_x, _y+1))/4;

			//std::cout << "M";
//	}

    //change = ((value - oldValue)>0)?(value - oldValue):(oldValue - value);
    change = abs(value-oldValue);

    //if(change >0) std::cout << "\nchange: " << change << std::endl;

    //if (change < 0.01) this->reduceLifeForce();

	_myAether.setChangeValue(_x, _y, change);
	_myAether.setFieldValue(_x, _y, value);
	this->reduceLifeForce(change);

	return;

	}

};


