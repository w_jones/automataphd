/*
 * aether.h
 *
 *  Created on: May 15, 2015
 *      Author: jonesey
 */

#ifndef AETHER_H_
#define AETHER_H_

#include "Property.h"
#include <iostream>
#include "arraySize.h"
#include <iostream>
#include <iomanip>

//using namespace std;


class Aether
{
public:
	//default constructor
	Aether();

	//construct an aether object that is m x n
	Aether(int m,int n);

	//initialize the Aether to a value
	void initializeAetherFieldValue(float initialValue);

	//return the population of the aether at location (x, y)
	int getPopulation(int x, int y);

	//set population of the aether at location (x, y)
	void setPopulation(int x, int y, int val);

	//return the neighbors of the aether at location (x, y)
	int getNeighbors(int x, int y);

	//set neighbors of the aether at location (x, y)
	void setNeighbors(int x, int y, int val);

	//return the field value of the aether at location (x, y)
	float getFieldValue(int x, int y);

	//set field value of the aether at location (x, y)
	void setFieldValue(int x, int y, float val);

	//return the change value of the aether at location (x, y)
	float getChangeValue(int x, int y);

	//set change value of the aether at location (x, y)
	void setChangeValue(int x, int y, float val);

	//return the immunity value of the aether at location (x, y)
	int getImmunity(int x, int y);

	//set the immunity value of the aether at location (x, y)
	void setImmunity(int x, int y, int val);

	//set width of array
	void setWidth(int x);

	//get width of aether
	int getWidth();

	//set height of array
	void setHeight(int y);

	//get height of array
	int getHeight();

	//plus - adds neighbors and population when a cell directs it to do so
	//this might be better in the 'cell' class, but it's a matter of perspective.
	//could be worthwhile to see if there is a run-time difference
	void plus(int x, int y);

	//minus - subtracts neighbors and population when a cell directs it to do so
	//this might be better in the 'cell' class, but it's a matter of perspective.
	//could be worthwhile to see if there is a run-time difference
	void minus(int x, int y);

	//this function sums the values in the 'changeValue' field of the Aether and
	//reports it.
	float checkChangeValues();

	//display the aether - for debugging mostly
	void displayFieldValues();


//private:

	Property<int, global_x_dim > population;
	Property<int, global_x_dim > neighbors;
	Property<float, global_x_dim > fieldValue;
	Property<float, global_x_dim > changeValue;
	Property<int, global_x_dim > immunity;
	Property<float, global_x_dim > lifeForce;



	int width;
	int height;

	void initializeAether(int x, int y);

};


#endif /* AETHER_H_ */
