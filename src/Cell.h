/*
 * cell.h
 *
 *  Created on: May 22, 2015
 *      Author: jonesey
 */

#ifndef CELL_H_
#define CELL_H_

#include "Property.h"
#include <iostream>
#include <cmath>
#include <cstdlib>
#include "arraySize.h"
#include "Aether.h"
#include <cstdio>
#include <iomanip>

class Cell
{
public:
	Cell();
	Cell(int _x, int _y);
	Cell(int _x, int _y, bool _state);
	void live();
	void sleep();
	int checkNeighbors(int _x, int _y, Aether _myAether);
	int checkPopulation(int _x, int _y, Aether _myAether);
	float checkChangeValue(int _x, int _y, Aether _myAether);
	void setLifeForce(float _value);
	void reduceLifeForce(float _change);
	float getLifeForce();
	void linOp(int _x, int _y, Aether _myAether);
	int getX();
	int getY();


private:
	int x;
	int y;
	bool state;
	float lifeForce;
};

#endif /* CELL_H_ */
