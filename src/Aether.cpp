/*
 * aether.cpp
 *
 *  Created on: May 15, 2015
 *      Author: jonesey
 */

#include "Aether.h"
using namespace std;

Aether::Aether(){


	int defaultSize = global_x_dim;

	this->width = 0;
	this->height = 0;

	this->setWidth(defaultSize);
	this->setHeight(defaultSize);

	//initializeAether(defaultSize, defaultSize);

	for (int i = 0; i < defaultSize; i++){
		for (int j = 0; j < defaultSize; j++){
			this->setPopulation(i,j,0);
			this->setChangeValue(i,j,0.0);
			this->setNeighbors(i, j, 0);
			this->setImmunity(i,j,0);
		} //endfor j
	}//endfor i

}

void Aether::initializeAetherFieldValue(float _initialValue){

	int x = global_x_dim;
	int y = global_y_dim;

	for (int i = 0; i < x; i++)
		for (int j = 0; j < y; j++)
			this->setFieldValue( i, j, _initialValue);

}

void Aether::setPopulation(int _x, int _y, int _val){

	this->population.setValue(_x,_y,_val);
}

int Aether::getPopulation(int _x, int _y){

	return this->population.getValue(_x,_y);
}

void Aether::setNeighbors(int _x, int _y, int _val){

	this->neighbors.setValue(_x,_y,_val);
}

int Aether::getNeighbors(int _x, int _y){

	return this->neighbors.getValue(_x,_y);
}

void Aether::setFieldValue(int _x, int _y, float _val){

	this->fieldValue.setValue(_x,_y,_val);
}

float Aether::getFieldValue(int _x, int _y){

	return this->fieldValue.getValue(_x,_y);
}

void Aether::setChangeValue(int _x, int _y, float _val){

	this->changeValue.setValue(_x,_y,_val);
}

float Aether::getChangeValue(int _x, int _y){

	return this->changeValue.getValue(_x,_y);
}

void Aether::setImmunity(int _x, int _y, int _val){

	this->immunity.setValue(_x,_y,_val);
}

int Aether::getImmunity(int _x, int _y){

	return this->immunity.getValue(_x,_y);
}

void Aether::setWidth(int _x){

	this->width = _x;
}

int Aether::getWidth(){

	return this->width;
}

void Aether::setHeight(int _y){

	this->height = _y;
}

int Aether::getHeight(){

	return this->height;
}

//The plus function is the Aether's response wrt automata
//when a cell is created at (x,y)
void Aether::plus(int _x, int _y){


	//increment each neighbor around x,y (including x,y)
	for (int i = -1; i < 2; i++)
		for (int j = -1; j < 2; j++)
			this->neighbors.increment(_x+i, _y+j);
	//remove the (x,y) neighbor increment - can't be your own neighbor
	this->neighbors.decrement(_x,_y);

	//add one to the population in the current cell
	this->population.increment(_x,_y);
};

//The minus function is the Aether's response wrt automata
//when a cell is destroyed at (x,y)
void Aether::minus(int _x, int _y){


	//decrement each neighbor around x,y (including x,y)
	for (int i = -1; i < 2; i++)
		for (int j = -1; j < 2; j++)
			this->neighbors.decrement(_x+i, _y+j);
	//add the local effect back in - can't be your own neighbor
	this->neighbors.increment(_x,_y);

	//subtract one from the population in the current cell
	this->population.decrement(_x,_y);
}

float Aether::checkChangeValues(){

	float summation = 0.0;

	//cout << "\nin Aether::checkChangeValues";

	for (int i = 0; i< this->width; i++)
		for (int j = 0; j < this->height; j++){
			summation += this->getChangeValue(i,j);
		}

	//cout << "\nsummation = " << summation;
	return summation;

}

void Aether::displayFieldValues(){
	for (int i = 0; i< this->width; i++){
		cout << endl;
		for (int j = 0; j < this->height; j++){
			std::cout << scientific << setw( 11 ) << setprecision( 6 ) << this->getFieldValue(i,j) << ' ';
		}
	}
}








