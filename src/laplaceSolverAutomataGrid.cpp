//============================================================================
// Name        : laplaceSolverAutomataGrid.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "Property.h"
#include "Aether.h"
#include "Cell.h"
#include "nSquaredSolution.h"

#include <cmath>
#include <list>
#include <iterator>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <iomanip>


#include "arraySize.h"

using namespace std;

//function prototypes

void initializeAetherFieldValue(Aether, float);
void initializeAetherImmunity(Aether);
void populateCells(list<Cell>&, Aether);
void processCells(list<Cell>&, Aether, float*);
void processAndGrowCells(list<Cell>&, Aether);
void processAndRemoveCells(list<Cell>&, Aether, int);
float checkConvergenceAgainstSolution(nSquaredSolution, Aether);

int main() {


	float currentChangeValue = 1.0;
	float changeCheckValue = 0.0;
	int cadence = 0;
	float oldChangeValue = 0;
	float convergenceValue = 1000.0;
	int iteration = 0;

	Aether* my_Aether = new Aether();

	nSquaredSolution* my_nSqSol = new nSquaredSolution(global_x_dim);

	//my_nSqSol->displayMatrix();
	char tempChar;
	//cin >> tempChar;

	typedef std::list<Cell> my_Cell_List;

	my_Cell_List cellListInstance;

	//Initializing some values

	cout << "\n!!!Here we Gooooo!!!\n" << endl;

	//This determines the solution using a 'typical relaxation' method
	int timeStart = time(0);
	my_nSqSol->solveIt();
	int timeEnd = time(0);
	cout << "\nThat took " << timeEnd-timeStart << " seconds.";

	//my_nSqSol->displayMatrix();
	cout << "\ntype character and enter to go: ";
	cin >> tempChar;

	//Next we initialize the Aether fields

	initializeAetherFieldValue(*my_Aether, 0.0);

	//cin >> tempChar;

	initializeAetherImmunity(*my_Aether);

	//Create some cells

	populateCells(cellListInstance, *my_Aether);

	//need some convergence criterion here
	// WHILE NOT CONVERGED

	//while (currentChangeValue > 0.00001){

	//my_Aether->displayFieldValues();
	//cin >> tempChar;

	//int iterations = 0;
	cout << "\nout of populateCells loopb\n";

//	while (iterations < 100){

	timeStart = time(0);

	int automataIterations = 0;

	while (convergenceValue > 0.001){


		iteration++;
		cadence++;

		automataIterations+=cellListInstance.size();
		processCells(cellListInstance, *my_Aether, &currentChangeValue);
		automataIterations+=cellListInstance.size();
		processAndGrowCells(cellListInstance, *my_Aether);
		//cout << endl;
		automataIterations+=cellListInstance.size();
		processCells(cellListInstance, *my_Aether, &currentChangeValue);
		automataIterations+=cellListInstance.size();
		processAndRemoveCells(cellListInstance, *my_Aether, iteration);
		automataIterations+=cellListInstance.size();
		processCells(cellListInstance, *my_Aether, &currentChangeValue);
		////ONLY CHECK AT AUTOMATA LOCATIONS
		//if(cadence%5==0){
			convergenceValue = checkConvergenceAgainstSolution(*my_nSqSol, *my_Aether)/global_x_dim/global_y_dim;

		//cout << endl;

		//if (currentChangeValue < 1.0e-50 || cellListInstance.size()<200)
		if (cellListInstance.size()<400){

			//if (cadence == 1000){
			cout << "\n convergenceValue: ";
						cout << scientific << setw( 11 ) << setprecision( 6 ) << convergenceValue;
						cout << "\t\t#cells: " << cellListInstance.size();
						cout << "\tcurrent life force threshold: " << exp(-1*log((float)iteration/global_x_dim/global_x_dim));
		  //  if (currentChangeValue == 0.0){
			cadence = 0;
			//cout << "\n\n*******************************\n\n";
			populateCells(cellListInstance, *my_Aether);

		    //}

			/*cout << "\ncontinue - y/n ";
			cin >> tempChar;

			for( my_Cell_List::iterator listMyClassIter = cellListInstance.begin(); // not listMyClass.begin()
							    listMyClassIter != cellListInstance.end(); // not listMyClass.end()
							    listMyClassIter ++)
							{
							    cout << endl << (*listMyClassIter).getLifeForce();
							}

			cout << "\ncontinue - y/n ";
			cin >> tempChar;
			*/

		}
		//}
	//iterations++;
	/*
	while(cadence < 15){

		cout << endl << cadence++ << endl;

		cadence = cadence % 15;

		if(cadence%15==0){
			//changeCheckValue = changeCheckValue - currentChangeValue;
			if (changeCheckValue != 0.0) changeCheckValue = currentChangeValue;
			else {
				populateCells(cellListInstance, *my_Aether);
				cout << "\nRandomizing Cells\n";
				//break; //DEBUG BREAK so it only goes through once
			}
		}

		if(cadence%2==0){
			processAndGrowCells(cellListInstance, *my_Aether);
			//cout << "\t\t#cells: " << cellListInstance.size();
		}

		if((cadence+1)%2==0){
			processAndRemoveCells(cellListInstance, *my_Aether);
			//cout << "\t\t#cells: " << cellListInstance.size();
		}

		oldChangeValue = currentChangeValue;
		processCells(cellListInstance, *my_Aether, &currentChangeValue);

		//if(oldChangeValue - currentChangeValue == 0.0) cadence = 150;



		cout << "\n currentChangeValue: ";
		cout << scientific << setw( 11 ) << setprecision( 6 ) << currentChangeValue;
		cout << "\t\t#cells: " << cellListInstance.size();

        convergenceValue = checkConvergenceAgainstSolution(*my_nSqSol, *my_Aether);

	}
*/
	}

    // ENDWHILE

	my_Aether->displayFieldValues();

	timeEnd = time(0);

	cout << "\nThat took " << timeEnd - timeStart << " seconds. and " << iteration << " automataIterations.";

	cin >> tempChar;

	for( my_Cell_List::iterator listMyClassIter = cellListInstance.begin(); // not listMyClass.begin()
	    listMyClassIter != cellListInstance.end(); // not listMyClass.end()
	    listMyClassIter ++)
	{
	    //cout << endl << (*listMyClassIter).getLifeForce();

	}



	return 0;
}

//functions

void initializeAetherFieldValue(Aether _iafvAether, float _initialValue){

	//this will initialize the aether to a flat value (provided)
	//we might want  to initialize it to a function at some point...

	cout << "\nin initializeAetherFieldValue\n";

	_iafvAether.initializeAetherFieldValue(_initialValue);

	int x = _iafvAether.getWidth();
	cout << "width of aether is " << x;
	int y = _iafvAether.getHeight();

	//going to set top edge to 1 - it's arbitrary at this point

	for (int i = 0; i < x ;i++)
		_iafvAether.setFieldValue( 0, i, 1.0);
	//cout << endl << "aether field value (0, 0) = " << _iafvAether.getFieldValue(0,0) << endl;

}


void initializeAetherImmunity(Aether _iaiAether){

	int x = _iaiAether.getWidth();
	int y = _iaiAether.getHeight();

	cout << "\nin initializeAetherImmunity\n";

	//there are lots of options to initialize the aether's boundary conditions
	//right now I'm going to set the immunity of the top edge to preserve the BC

		for (int j = 0; j < x ;j++){
			_iaiAether.setImmunity(0, j, 1);
			_iaiAether.setImmunity(y - 1, j, 1);
		}

		for (int i = 0; i < y ; i++){
			_iaiAether.setImmunity(i, 0, 1);
			_iaiAether.setImmunity(i, x - 1, 1);
		}


}

void populateCells(list<Cell>& _popList, Aether _pcAether){

	int x = _pcAether.getWidth();
	int y = _pcAether.getHeight();

	//cout << "\nin populate cells loop\n";

	srand(time(0));

	//take the edges out for now...
	for (int i = 1; i < x-2; i++)
		for (int j = 1; j < y-2; j++){
			int testRand = rand()%1000;
			//cout << endl << testRand << endl;
			if(testRand<100 && _pcAether.getPopulation(i,j)==0){
				_popList.push_back(Cell(i,j));
				//maybe this goes in the aether...? in the cell? maybe not.
			    _pcAether.plus(i,j);
			}
		}

}

void processCells(list<Cell>& _linOpList, Aether _loAether, float* CCVpointer){

	//cout << "\nin processCells.";

	*CCVpointer = 0.0;

	for(std::list<Cell>::iterator listMyClassIter = _linOpList.begin(); // not listMyClass.begin()
		    listMyClassIter != _linOpList.end(); // not listMyClass.end()
		    listMyClassIter ++)
		{
		    //this could probably be called much more efficiently by returning an array?
		    int x = (*listMyClassIter).getX();
		    int y = (*listMyClassIter).getY();
		    (*listMyClassIter).linOp(x, y, _loAether);//performs the linear operation
		    *CCVpointer += _loAether.getChangeValue(x,y);
		}

}

void processAndGrowCells(list<Cell>& _linOpList, Aether _loAether){

	//cout << "\nin procesAndGrowCells.";

	for(std::list<Cell>::iterator listMyClassIter = _linOpList.begin(); // not listMyClass.begin()
		    listMyClassIter != _linOpList.end(); // not listMyClass.end()
		    listMyClassIter ++)
		{
		    //this could probably be called much more efficiently by returning an array?
		    int x = (*listMyClassIter).getX();
		    int y = (*listMyClassIter).getY();
		    (*listMyClassIter).linOp(x, y, _loAether);//performs the linear operation
		    //cout << _loAether.getNeighbors(x,y) ;

/*		    if(x == 1){
		    	for (int i = x; i < x+2; i++)
		    			    	for (int j = y-1; j < y+2; j++){
		    			    		if ((_loAether.getNeighbors( i, j)==3)
		    			    			&& _loAether.getPopulation( i, j)==0){
		    			    			_linOpList.push_front(Cell(i,j));
		    			    		    _loAether.plus(i,j);
		    			    		}
		    			    	}
		    }

		    else if (x == global_x_dim-2){
		    	for (int i = x-1; i < x+1; i++)
		    			    	for (int j = y-1; j < y+2; j++){
		    			    		if ((_loAether.getNeighbors( i, j)==3)
		    			    			&& _loAether.getPopulation( i, j)==0){
		    			    			_linOpList.push_front(Cell(i,j));
		    			    			_loAether.plus(i,j);
		    			    		}
		    			    	}
		    }

		    else if (y == 1){
		    	for (int i = x-1; i < x+2; i++)
		    		for (int j = y; j < y+2; j++){
		    			if ((_loAether.getNeighbors( i, j)==3)
		    					&& _loAether.getPopulation( i, j)==0){
		    					_linOpList.push_front(Cell(i,j));
		    					_loAether.plus(i,j);
		    			}
		    	}

		    }

		    else if (y == global_y_dim-2){
		    	for (int i = x-1; i < x+2; i++)
		    			    	for (int j = y-1; j < y+1; j++){
		    			    		if ((_loAether.getNeighbors( i, j)==3)
		    			    			&& _loAether.getPopulation( i, j)==0){
		    			    			_linOpList.push_front(Cell(i,j));
		    			    			_loAether.plus(i,j);
		    			    		}
		    			    	}
		    }

		    else{
*/
				for (int i = x-1; i < x+2; i++)
					for (int j = y-1; j < y+2; j++){
						if ((_loAether.getNeighbors( i, j)==3)
							&& _loAether.getPopulation( i, j)==0
							&& _loAether.getImmunity(i,j)==0)
						{
							_linOpList.push_front(Cell(i,j));
							_loAether.plus(i,j);
							//cout << "\nx: " << i << " y: " << j << "\n";
							//cout << '+';
						}
					}
//		    }
		}


}

void processAndRemoveCells(list<Cell>& _linOpList, Aether _loAether, int _iteration){

	float lifeThreshold = 1.0e-20;
	//cout << "\nin processAndRemoveCells.";

	for(std::list<Cell>::iterator listMyClassIter = _linOpList.begin(); // not listMyClass.begin()
		    listMyClassIter != _linOpList.end(); // not listMyClass.end()
		    )
		{
		    //this could probably be called much more efficiently by returning an array?
		    int x = (*listMyClassIter).getX();
		    int y = (*listMyClassIter).getY();
		    int myCellNeighbors = (*listMyClassIter).checkNeighbors(x,y,_loAether);
		    float myCellLifeForce = (*listMyClassIter).getLifeForce();

		    (*listMyClassIter).linOp(x, y, _loAether);//performs the linear operation


		    if ((myCellNeighbors > 3 && myCellLifeForce < lifeThreshold)  ||
		    	(myCellNeighbors < 2 && myCellLifeForce < lifeThreshold)  ||
		    	//myCellLifeForce < exp(-1*log((float)_iteration/global_x_dim/global_x_dim)))
		    	myCellLifeForce < lifeThreshold)
		    	{
		    	listMyClassIter = _linOpList.erase(listMyClassIter);
		    	_loAether.minus(x,y);
		    	//cout << 'o';
		    	}

	    	++listMyClassIter;
		}
      //cout << "\nend process and remove cells.\n";

}

float checkConvergenceAgainstSolution(nSquaredSolution _nSqSol, Aether _Aether){

	float accumulator = 0.0;

	for (int i = 0; i < global_x_dim; i++)
		for (int j = 0; j < global_y_dim; j++){
			//cout << endl << _nSqSol.getSolutionMatrix()[i][j] << " : " << _Aether.getFieldValue(i,j);
			accumulator += abs(_nSqSol.getSolutionMatrix()[i][j]-_Aether.getFieldValue(i,j));
		}
	//cout << endl << accumulator;
	return accumulator;
}
