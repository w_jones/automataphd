/*
 * nSquaredSolution.h
 *
 *  Created on: Jul 18, 2015
 *      Author: jonesey
 */

#ifndef NSQUAREDSOLUTION_H_
#define NSQUAREDSOLUTION_H_

//============================================================================
// Name        : laplaceSolverRegular.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include <cstdlib>
#include <cmath>
using namespace std;

class nSquaredSolution
{
public:
	nSquaredSolution();
	nSquaredSolution(int);
	void solveIt();
	float** getSolutionMatrix();
	void displayMatrix();

private:
	int matrixSize;
	float ** solutionMatrix;
	float residual;
	void InitializeMatrix(int);
	float solveMatrix(int, float**);
	void displayMatrixPrivate();
};

#endif /* NSQUAREDSOLUTION_H_ */
