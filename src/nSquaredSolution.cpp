/*
 * nSquaredSolution.cpp
 *
 *  Created on: Jul 18, 2015
 *      Author: jonesey
 */

#include "nSquaredSolution.h"
#include <iostream>
#include <cstdio>

//public functions

nSquaredSolution::nSquaredSolution(){
	this->matrixSize = 100;
	this->residual = 1.0;
	this->InitializeMatrix(this->matrixSize);
}

nSquaredSolution::nSquaredSolution(int _size){
	this->matrixSize = _size;
	this->residual = 1.0;
	this->InitializeMatrix(this->matrixSize);
}

void nSquaredSolution::solveIt(){
	long int nSquaredIteration = 0;
	while (this->residual > 0.00000001){
		nSquaredIteration+=this->matrixSize*this->matrixSize;
		cout << endl << this->residual;
		this->residual = solveMatrix(matrixSize,solutionMatrix);
	}
	cout << "\n that took " << nSquaredIteration << " iterations.\n";
}

float** nSquaredSolution::getSolutionMatrix(){
	return this->solutionMatrix;
}

//private functions

void nSquaredSolution::InitializeMatrix(int _size){
	float** tempArray = new float*[_size];
	for(int i = 0; i<_size;i++)
		tempArray[i] = new float[_size];

	this->solutionMatrix = tempArray;

	for (int j = 0; j < _size; j++){
	    	this->solutionMatrix[0][j]=1.0;
	    }

	    for (int i = 1; i < _size; i++){
	    	for (int j = 0; j < _size; j++){
	    		this->solutionMatrix[i][j] = 0.0;
	    	}
	    }

	    cout << "\nSquaredSolution Initialized.\n\n";
}

float nSquaredSolution::solveMatrix(int matrixSize, float **matrix) {

	float residual = 0;

    for (int i = 1; i< matrixSize -1 ; i++){
    	for (int j = 1; j<matrixSize -1; j++){
    		float oldEntry = matrix[i][j];
    		matrix[i][j] = (matrix[i-1][j]+
    				        matrix[i+1][j]+
    				        matrix[i][j-1]+
    				        matrix[i][j+1])/4;
    		residual+=pow((oldEntry-matrix[i][j]),2);
    	}
    }

    cout << ".";
	return pow(residual,0.5f);

}

void nSquaredSolution::displayMatrix(){
	this->displayMatrixPrivate();
}

void nSquaredSolution::displayMatrixPrivate() {

    for (int i = 0; i< this->matrixSize ; i++){
    	cout << endl;
    	for (int j = 0; j<this->matrixSize; j++){
    		cout << this->solutionMatrix[i][j];
    	}
    }
    cout << endl;

	return;
}


