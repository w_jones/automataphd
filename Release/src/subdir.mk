################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Aether.cpp \
../src/Cell.cpp \
../src/Property.cpp \
../src/laplaceSolverAutomataGrid.cpp \
../src/nSquaredSolution.cpp 

OBJS += \
./src/Aether.o \
./src/Cell.o \
./src/Property.o \
./src/laplaceSolverAutomataGrid.o \
./src/nSquaredSolution.o 

CPP_DEPS += \
./src/Aether.d \
./src/Cell.d \
./src/Property.d \
./src/laplaceSolverAutomataGrid.d \
./src/nSquaredSolution.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


